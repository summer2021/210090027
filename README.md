# 1. 中期报告与结项报告

> 中期结项报告: [中期报告](mid_report.md)
> 
> 结项报告: [终期报告](final_report.md)


# 2. 结项完成情况: 

1. `发布版本时，自动部署文档到 ShardingSphere-doc 仓库`: 

| 仓库名 | 分支名 | Pull Request  |
| - | - | -  |
| https://github.com/QiliangFan/shardingsphere-doc | new-doc | https://github.com/apache/shardingsphere-doc/pull/557 \| https://github.com/apache/shardingsphere-doc/pull/558  |

2. `发布版本时，自动发布镜像到 Registry`:

| 仓库名 | 分支名 | Pull Request  |
| - | - | -  |
| https://github.com/QiliangFan/shardingsphere | docker | https://github.com/apache/shardingsphere/pull/12414 |

3. `自动化 issue 标准处理流程`:

| 仓库名 | 分支名 | Pull Request  |
| - | - | -  |
| https://github.com/QiliangFan/shardingsphere | issue-actions | https://github.com/apache/shardingsphere/pull/12241 |

# 3. 源码:

> 中期之后由于有比较多的测试提交和测试分支, 因此最终提交pull request的分支和中期上传的分支不同.
>
> 目前已经从提交分支上将代码同步到此仓库中, 按照任务顺序, 分支分别为: new-doc、docker和issue-actions。
