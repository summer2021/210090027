> 开发方式是通过fork github仓库进行本地开发, 代码请移步: 
>
> https://github.com/QiliangFan/shardingsphere
>
> https://github.com/QiliangFan/shardingsphere-doc

此仓库中代码仅为相关GitHub仓库中开发分支的镜像:

> `asf-site` : 该分支用于开发`文档自动部署`需求
>
> `issue`: 该分支用于开发`issue流程标准化`需求
>
> `deploy`: 该分支用于开发`镜像自动发布`需求 

# :hammer: 项目信息:

#### :gear: 项目名称: [通过 Github Actions 实现标准流程自动化](https://summer.iscas.ac.cn/#/org/prodetail/210090027?lang=en)

#### :ledger: 方案描述:

> 该项目主要分为三个部分

1. :bookmark_tabs: 发布版本时，自动部署文档到ShardingSphere-doc仓库

在shardingsphere-doc中，有一个定时的工作流，会定时的更新master分支的文档，我们在此基础上增加一个判断，获取所有tag列表，如果对应tag的文档尚未生成，则触发文档构建逻辑构建相应文档，更新到ShardingSphere-doc仓库中。

![文档自动发布](https://z3.ax1x.com/2021/05/13/gBRkuQ.png)

2. :pushpin: 发布版本时，自动发布镜像到Registry

在shardingshpere中，需要通过工作流在版本发布时自动将将若干个组件的Docker镜像推送到镜像仓库中。

3. :speech_balloon: 自动化issue标准化流程

在shardingshpere中，issue的管理目前还是通过人工进行，现在需要通过工作流，来自动的对各种issue及评论进行管理。

#### :hourglass_flowing_sand: 时间规划：

| 时间段        | 事件                                                         |
| ------------- | ------------------------------------------------------------ |
| 07.01 ~ 07.10 | 编写文档自动化部署相关shell脚本和工作流yaml文件              |
| 07.10 ~ 07.20 | 编写issue自动化管理相关的工作流yaml文件                      |
| 07.30 ~ 08.10 | 完成ShardingSphere的docker镜像的自动推送的工作流yaml文件     |
| 08.10 ~ 08.15 | 导师代码review及自测完善代码                                 |
| 08.15 ~ 08.30 | 根据导师review结果，修改代码并自测，发起PR，完成项目基本要求 |
| 08.30 ~ 09.15 | 输出文档，并且根据代码检查结果修改代码提交补丁减少代码中的缺陷 |
| 09.15 ~ 09.30 | 完善文档、持续测试、修补代码缺陷                             |

# :calendar: 项目进度：

> 三个功能分别用三个分支来完成的，由于shardingsphere的此项目设计两个仓库，因此我分别fork了两个本地仓库，共三个开发分支，若干测试分支）
>
> 1. 文档自动发布：https://github.com/QiliangFan/shardingsphere-doc [branch: asf-site]
> 2. 自动发布镜像：https://github.com/QiliangFan/shardingsphere [branch: deploy]
> 3. issue流程标准化：https://github.com/QiliangFan/shardingsphere [branch: issue]

 :one:1. 项目研发第一阶段（07 月 01 日 - 08 月 15 日）：（自测阶段）

- [x] 完成文档自动化部署任务，将实时更新的文档与ShardingSphere版本发布相关文档内容的分离。

- [x] 完成issue自动化管理：
  - [x] 自动关闭包含指定标签的issue
  - [ ] issue自动回复（需要在下一阶段和导师讨论）
  - [ ] 自动指派（由于仓库的issue 委派者有轮班， 暂不实现）
  - [x] 检查不活跃issue并添加相应标签
  - [x] 回复`/run ci` 执行工作流`CI`
- [x] 完成ShardingSphere的docker镜像向registry推送。

:two: 2. 项目研发第二阶段（08 月 16 日 - 09 月 30 日）：（review、代码合并阶段）

- [ ] 解决在中期验收阶段中发现的问题
- [ ] 对分支进行自测、根据mentor的review进行修改完善，发起PR完成基本任务
- [ ] 对第一阶段完成的内容进行更详细的测试
- [ ] 对第一阶段的完成内容进行总结，并输出相关文档内容
- [ ] 思考可以改进或者补充的地方

### 已完成工作

- `文档自动发布`、`镜像自动发布`和`issue流程标准化`已于本地初步开发完毕（具体可见相关仓库和分支）且在测试分支上初步测试完毕

  - 文档自动发布：在原先工作流的基础上，增加了对git仓库的tag的获取，若有对应tag版本的文档可生成且尚未构建，则触发构建文档的shell脚本，构建相应tag版本的文档，且使用sed指令在html文件中插入相关超链接，以指向相应版本的文档
  - 镜像自动发布：由于镜像的Dockerfile已经编写好，且Maven的构建目标中也有相应的Docker镜像，因此该任务只需完成工作流即可，下面是相关工作流：

  ```yaml
  #
  # Licensed to the Apache Software Foundation (ASF) under one or more
  # contributor license agreements.  See the NOTICE file distributed with
  # this work for additional information regarding copyright ownership.
  # The ASF licenses this file to You under the Apache License, Version 2.0
  # (the "License"); you may not use this file except in compliance with
  # the License.  You may obtain a copy of the License at
  #
  #     http://www.apache.org/licenses/LICENSE-2.0
  #
  # Unless required by applicable law or agreed to in writing, software
  # distributed under the License is distributed on an "AS IS" BASIS,
  # WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  # See the License for the specific language governing permissions and
  # limitations under the License.
  #
  
  name: Deploy Document And Docker Image
  
  on:
    release:
      types:
        - published
    workflow_dispatch:
  
  env:
    MAVEN_OPTS: -Dhttp.keepAlive=false -Dmaven.wagon.http.pool=false -Dmaven.wagon.http.retryHandler.count=3 -Dmaven.wagon.httpconnectionManager.ttlSeconds=30 -DskipTests
  
  jobs:
    deploy-document:
      runs-on: ubuntu-latest
      timeout-minutes: 60
      steps:
        - uses: actions/checkout@v2
  
        # build the document relative to release version
        - name: Setup Workflow Dispatch To Build Document
          uses: mvasigh/dispatch-action@main
          with:
            token: ${{ secrets.PERSONAL_TOKEN }}
            repo: shardingsphere-doc
            owner: ${{ github.actor	}}
            event_type: update
  
    deploy-docker-iamge:
      runs-on: ubuntu-latest
      timeout-minutes: 60
      steps:
        - uses: actions/checkout@v2
        # build target with maven
        - name: Cache Maven Repos
          uses: actions/cache@v2
          with:
            path: ~/.m2/repository
            key: ${{ runner.os }}-maven-${{ hashFiles('**/pom.xml') }}
            restore-keys: |
              ${{ runner.os }}-maven-
        - name: Set up JDK 8
          uses: actions/setup-java@v1
          with:
            java-version: 8
        - name: set environment
          run: export MAVEN_OPS=' -Dmaven.javadoc.skip=true -Drat.skip=true -Djacoco.skip=true'
        - name: Build Project
          run: ./mvnw -B -Prelease,docker -DskipTests clean install
  
        # build and pubilsh image
        - name: Set up QEMU
          uses: docker/setup-qemu-action@v1
  
        - name: Set up Docker Buildx
          uses: docker/setup-buildx-action@v1
  
        - name: Login to DockerHub
          uses: docker/login-action@v1
          with:
            username: ${{ secrets.DOCKERHUB_USERNAME }}
            password: ${{ secrets.DOCKERHUB_TOKEN }}
  
        - name: Push Docker Image
          run: |
            echo Docker Images: 
            echo `docker image ls|grep apache|awk '{print $1":"$2}'`
            docker image ls|grep apache|awk '{print $1":"$2}'|xargs -i docker push {}
  ```

  - issue流程标准化：此任务也只需创建工作流，并在其中响应各类issue相关事件、检查issue状态即可。

  ```yaml
  #
  # Licensed to the Apache Software Foundation (ASF) under one or more
  # contributor license agreements.  See the NOTICE file distributed with
  # this work for additional information regarding copyright ownership.
  # The ASF licenses this file to You under the Apache License, Version 2.0
  # (the "License"); you may not use this file except in compliance with
  # the License.  You may obtain a copy of the License at
  #
  #     http://www.apache.org/licenses/LICENSE-2.0
  #
  # Unless required by applicable law or agreed to in writing, software
  # distributed under the License is distributed on an "AS IS" BASIS,
  # WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  # See the License for the specific language governing permissions and
  # limitations under the License.
  #
  
  name: ISSUE Standardized Process
  
  on:
    schedule:
      - cron: '0 0 */1 * *'  # once a day
  
    issue_comment:
      types: [created,edited]
  
    issues:
      types: [labeled,edited,reopened]
    
  
  jobs:
    comment-action:
      runs-on: ubuntu-latest
      if: ${{ github.event_name == 'issue_comment' }}
      steps:
        - name: Print Issue Comment
          run: |
            echo ${{ github.event.comment.body }}
  
        - name: Trigger The CI Workflow
          if: ${{ contains( github.event.comment.body , '/run ci') }} 
          uses: mvasigh/dispatch-action@main
          with:
            token: ${{ secrets.PERSONAL_TOKEN }}
            repo: shardingsphere
            owner: ${{ github.actor }}
            event_type: rerun-ci
  
    remove-invalid:
      runs-on: ubuntu-latest
      if: ${{ github.event_name == 'issue_comment' || ( github.event_name == 'issues' && ( github.event.action == 'edited' || github.event.action == 'reopened' ) ) }}
      steps:
      - name: Remove "status:invalid" label
        if: ${{ contains( github.event.issue.labels.*.name , format('status{0} invalid', ':') ) }}
        uses: actions-cool/issues-helper@v2.2.1
        with:
          actions: 'remove-labels'
          token: ${{ secrets.GITHUB_TOKEN }}
          issue-number: ${{ github.event.issue.number }}
          labels: "status: invalid"
  
    
    check-inactive-issue:
      runs-on: ubuntu-latest
      concurrency: check-inactive  # singleton-run-stage
      if: ${{ github.event_name == 'schedule' }}
      steps:
        - name: check-inactive   # add `status: invalid` label
          uses: actions-cool/issues-helper@v2.2.1 
          with: 
            actions: 'check-inactive' 
            token: ${{ secrets.GITHUB_TOKEN }} 
            inactive-day: 15
            inactive-label: "status: invalid"
            body: |
              Hello ${{ github.event.issue.user.login }}, this issue has not received a reply for serveral days.
              This issue is supposed to be closed.
            contents: "heart"
    
    close-issue:
      runs-on: ubuntu-latest
      concurrency:    # singleton-run-stage
        group: close-issue
        cancel-in-progress: true
      if: ${{ github.event_name == 'schedule' || ( github.event_name == 'issues' && github.event.action == 'labeled' ) }}
      steps:
        - name: Close Issue   # close issues with `labels`
          uses: actions-cool/issues-helper@v2.2.1
          with:
            actions: 'close-issues'
            token: ${{ secrets.GITHUB_TOKEN }}
            labels: 'status: invalid'
  ```

  

- `文档自动发布`工作方面导师已经review并提出了若干修改建议，已基本修改完毕。将根据导师的建议持续修改，最终合并代码到社区仓库

### 遇到的问题和解决方案:

- 这个项目涉及的主要方面是shell以及github actions. 因为CI/CD基本的还是操作系统中的一些基本操作,各种工具只是在其上的封装, 为了能够实现我们所需做的文档自动发布、issue流程标准化、镜像自动发布，利用github actions的事件触发和条件判断以及上下文环境的获取，我们仍需使用一些操作系统提供的命令行接口和其他命令行程序来实现我们所需要完成的构建、发布功能。
- github actions的优点在于提供了一个云构建、发布环境，能够将各种集成步骤放在云上，做到真正的自动化。这一点也是我对这个项目喜欢的原因，github actions能做的远不仅是CI/CD, 它就像一个微型的服务器, 可以执行各式各样的定时任务.
- 在编写github actions的工作流的过程中, 我也记录和总结了一些格式或者代码定式: :wine_glass: [torch-fan的wiki](https://wiki.torch-fan.site/github-actions-workflow-yu-fa/)

### 后续安排:

目前主要代码编写完毕, 后续主要工作是完善功能--查缺补漏, 完善代码使之符合要求, 因此接下来主要工作是根据导师的review结果修改代码最后合并分支.
