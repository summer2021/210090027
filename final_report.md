# 1. 项目信息 :film_projector:

##  :taco: 项目名称: `通过 Github Actions 实现标准流程自动化`

## :notebook: 方案描述:

- 发布版本时，自动部署文档到ShardingSphere-doc仓库
- 发布版本时，自动发布镜像到Registry
- 自动化issue标准处理流程

## :timer_clock: 时间规划：

> 该项目主要分为三个部分

1. :bookmark_tabs: 发布版本时，自动部署文档到ShardingSphere-doc仓库

在shardingsphere-doc中，有一个定时的工作流，会定时的更新master分支的文档，我们在此基础上增加一个判断，获取所有tag列表，如果对应tag的文档尚未生成，则触发文档构建逻辑构建相应文档，更新到ShardingSphere-doc仓库中。

2. :pushpin: 发布版本时，自动发布镜像到Registry

在shardingshpere中，需要通过工作流在版本发布时自动将将若干个组件的Docker镜像推送到镜像仓库中。

3. :speech_balloon: 自动化issue标准化流程

在shardingshpere中，issue的管理目前还是通过人工进行，现在需要通过工作流，来自动的对各种issue及评论进行管理。

#### :hourglass_flowing_sand: 时间规划：

| 时间段        | 事件                                                         |
| ------------- | ------------------------------------------------------------ |
| 07.01 ~ 07.10 | 编写文档自动化部署相关shell脚本和工作流yaml文件              |
| 07.10 ~ 07.20 | 编写issue自动化管理相关的工作流yaml文件                      |
| 07.30 ~ 08.10 | 完成ShardingSphere的docker镜像的自动推送的工作流yaml文件     |
| 08.10 ~ 08.15 | 导师代码review及自测完善代码                                 |
| 08.15 ~ 08.30 | 根据导师review结果，修改代码并自测，发起PR，完成项目基本要求 |
| 08.30 ~ 09.15 | 输出文档，并且根据代码检查结果修改代码提交补丁减少代码中的缺陷 |
| 09.15 ~ 09.30 | 完善文档、持续测试、修补代码缺陷                             |



# 2. 项目完成情况 :cake:

> 项目需求: https://github.com/apache/shardingsphere/issues/9697
>
> 完成情况概览: 
>
> ![image-20210918195536244](https://z3.ax1x.com/2021/09/23/4dR1IK.png)



## 1.  发布版本时, 自动部署文档到ShardingSphere-doc仓库 :bookmark_tabs:

:sailboat: 已提交的<font color="skyblue">**Pull Request**</font>:   

- https://github.com/apache/shardingsphere-doc/pull/557
- https://github.com/apache/shardingsphere-doc/pull/558



:tv: <font color="red">**结果展示**</font>: 目前代码已经在官网构建出了相应文档

![image-20210918193449921](https://z3.ax1x.com/2021/09/23/4dRsJS.png)



:tea: <font color='pink'>**测试**</font>结果: 

- 测试仓库: https://github.com/QiliangFan/shardingsphere-doc
- 测试分支: `test-doc`
- 提交分支: `new-doc`

- 为了更好的测试文档自动化构建, 依托GitHub Pages 和Cloudfare搭建了shardingsphere官网的一个镜像, 用于本地测试: https://shardingsphere-doc.torch-fan.site/
- 测试结果截图:

![image-20210918201054723](https://z3.ax1x.com/2021/09/23/4dRgMj.png)

![image-20210918201119989](https://z3.ax1x.com/2021/09/23/4dWPQH.png)





## 2. 发布版本时, 自动发布镜像到registry :rainbow:

:sailboat: 已提交的<font color="skyblue">**Pull Request**</font>:   

- https://github.com/apache/shardingsphere/pull/12414



:tv: <font color="red">**结果展示**</font>: 目前工作流在apache/shardingsphere仓库已经成功创建工作流

![image-20210918194841463](https://z3.ax1x.com/2021/09/23/4dWMlQ.png)



:tea: <font color='pink'>**测试**</font>结果: 

- 测试仓库: https://github.com/QiliangFan/shardingsphere
- 测试分支: `docker`
- 提交分支: `docker`
- 测试截图:

![image-20210918201403174](https://z3.ax1x.com/2021/09/23/4d4bh6.png)

![image-20210918201450888](https://z3.ax1x.com/2021/09/23/4d5vGV.png)



## 3. 自动化issue标准处理流程 :kick_scooter:

:sailboat: 已提交的<font color="skyblue">**Pull Request**</font>:   

- https://github.com/apache/shardingsphere/pull/12241



:tv: <font color="red">**结果展示**</font>: 目前工作流在apache/shardingsphere仓库已经成功创建工作流

![image-20210918195825389](https://z3.ax1x.com/2021/09/23/4dHi6K.png)

该需求目前有一个白名单问题, 实现ISSUE自动化管理借助了一些其他的actions，这些actions不在apache的白名单中，因此暂时会引起`startup failure`.  目前导师已经提起issue, 期待该问题的解决.



:tea: <font color='pink'>**测试**</font>结果: 

- 测试仓库: https://github.com/QiliangFan/shardingsphere
- 测试分支: issue-actions
- 提交分支: issue-actions
- 测试截图

![image-20210918201533093](https://z3.ax1x.com/2021/09/23/4dbqZF.png)



# 3. 遇到的问题 :bug:

- 外部GitHub Actions白名单问题：在使用GitHub Actions完成一些流程标准化时，需要自定义或者使用已经实现了的GItHub Actions，但是在一些仓库中（譬如Apache）的仓库下，设立了Actions的白名单，只有GitHub Actions官方的Actions和官方组织的Actions和一些在白名单中的Actions才能创建工作流。因此在实现需求中的部分功能时，有些功能虽然实现了但是无法正常启动。

- 由于是第一次参与开源项目的完成，在完成需求时小心翼翼，有时一个需求如果能够复用现有代码，就总是倾向于用现有代码尽最大可能避免出错，但是现有代码中有部分逻辑对目标需求是冗余的，导致实现需求时部分代码比较冗余。
- 交前几次Pull Request时，都是胆颤心惊的，提交PR后总是还会自己再看一两遍changes部分。突然接到GitHub邮件有新的comments看到有人评论代码缺陷或者代码中的错误，心情是尴尬又开心的。尴尬在于自己检查了几遍、测试了多次，代码中还是有一些问题，开心之处在于有人能够热心的给出提示给予帮助。



# 4. 项目完成质量: :hammer_and_pick:

​        每一项需求都经过了本地测试，并且通过搭建Shardingsphere的镜像网站来测试文档构建。尽最大可能在经过测试之后完成Pull Request的提交。但限于个人项目经验的局限，在代码风格和简洁度上有待提升。

​        与此同时，有时候测试一项功能时，始终无法正常通过测试，需要反复测试、调试十几次，BUG定位能力较差。







